package graf.gamefluids;

public class FluidRect {

	/**
	 * Amount of boxes in fist dimension excluding boundary boxes
	 */
	private final int X;

	/**
	 * Amount of boxes in first dimension including boundary boxes (X+2)
	 */
	public final int WIDTH;

	/**
	 * Amount of boxes in second dimension excluding boundary boxes
	 */
	private final int Y;

	/**
	 * Amount of boxes in second dimension including boundary boxes (X+2)
	 */
	public final int HEIGHT;
	
	/**
	 * Size of the entire array
	 */
	public final int size;

	private float[] u, v;
	private float[] u_prev, v_prev;

	private float[] dens;
	private float[] dens_prev;

	/**
	 * Creates a new 2-dimensional fluid field
	 * @param width
	 * @param height
	 */
	public FluidRect(int width, int height) {
		this.WIDTH = width;
		this.HEIGHT = height;
		X = width - 2;
		Y = height - 2;
		size = width * height;
		u = new float[size];
		v = new float[size];
		u_prev = new float[size];
		v_prev = new float[size];
		dens = new float[size];
		dens_prev = new float[size];
	}

	private void add_source(float[] x, float[] s, float dt) {
		int i;//, size = (N + 2) * (N + 2);
		for (i = 0; i < size; i++)
			x[i] += dt * s[i];
	}

	private void advect(int b, float[] d, float[] d0, float[] u, float[] v, float dt) {
		int i, j, i0, j0, i1, j1;
		float x, y, s0, t0, s1, t1, dt0;
		//dt0 = dt * N;
		dt0 = dt * (float)Math.sqrt(X*Y);//(X+Y)/2;
		// or sqrt(X*X+Y*Y)
		int srcIndex = 1+WIDTH;
		for (j = 1; j <= Y; j++) {
			for (i = 1; i <= X; i++) {
				//x = i - dt0 * u[IX(i, j)];
				//y = j - dt0 * v[IX(i, j)];
				x = i - dt0 * u[srcIndex];
				y = j - dt0 * v[srcIndex];
				if (x < 0.5)
					x = 0.5f;
				if (x > X + 0.5)
					x = X + 0.5f;
				i0 = (int) x;
				i1 = i0 + 1;
				if (y < 0.5)
					y = 0.5f;
				if (y > Y + 0.5)
					y = Y + 0.5f;
				j0 = (int) y;
				j1 = j0 + 1;
				s1 = x - i0;
				s0 = 1 - s1;
				t1 = y - j0;
				t0 = 1 - t1;
				d[srcIndex] = s0 * (t0 * d0[coordToIndex(i0, j0)] + t1 * d0[coordToIndex(i0, j1)])
						+ s1 * (t0 * d0[coordToIndex(i1, j0)] + t1 * d0[coordToIndex(i1, j1)]);
				srcIndex++;
			}
			srcIndex+=2;
		}
		set_bnd(b, d);
	}
	
	private void dens_step(
			float[] x, float[] x0, 
			float[] u, float[] v, 
			float diff,	float dt) {
		add_source(x, x0, dt);
		
		// SWAP ( x0, x );
		float[] tmp = x0;
		x0 = x;
		x = tmp;
		diffuse(0, x, x0, diff, dt);
		
		// SWAP ( x0, x );
		tmp = x0;
		x0 = x;
		x = tmp;
		
		advect(0, x, x0, u, v, dt);
		
	}
	
	private void diffuse(int b, float[] x, float[] x0, float diff, float dt) {
		int i, j, k;
		float a = dt * diff * X * Y;
		float divider = 1/(1+4*a);
		
		for (k = 0; k < 20; k++) {
			int srcIndex = 1*WIDTH+1;
			for (j = 1; j <= Y; j++) {
				for (i = 1; i <= X; i++) {
					//x[IX(i, j)] = (x0[IX(i, j)] + a
					//		* (x[IX(i - 1, j)] + x[IX(i + 1, j)] + x[IX(i, j - 1)] + x[IX(i, j + 1)]))
					
					x[srcIndex] = (x0[srcIndex] + a * (
							x[srcIndex-1] + 
							x[srcIndex+1] + 
							x[srcIndex-WIDTH] + 
							x[srcIndex+WIDTH])) * divider;
					/// (1 + 4 * a);
					srcIndex++;
				}
				srcIndex+=2;
			}
			
			set_bnd(b, x);
		}
	}
	
	/**
	 * Evaporates the density field by subtracting f*dt
	 * @param f
	 * @param dt
	 */
	public void evaporate(float f, float dt) {
		float sub = f*dt;
		for (int i=0;i<dens.length;i++) {
			dens[i] -= sub;
			if (dens[i]<0) dens[i]=0;
		}
	}

	/**
	 * Returns the density array. Array length is WIDTH*HEIGHT
	 * @return
	 */
	public float[] getDensity() {
		return dens;
	}

	/**
	 * Returns the density at position x/y
	 * @param x
	 * @param y
	 * @return
	 */
	public float getDensity(int x, int y) {
		return dens[coordToIndex(x, y)];
	}

	/**
	 * Returns the velocity x array. Array length is WIDTH*HEIGHT
	 * @return
	 */
	public float[] getVelocityX() {
		return u;
	}

	/**
	 * Returns the x velocity at position x/y
	 * @param x
	 * @param y
	 * @return
	 */
	public float getVelocityX(int x, int y) {
		return u[coordToIndex(x, y)];
	}

	/**
	 * Returns the velocity y array. Array length is WIDTH*HEIGHT
	 * @return
	 */
	public float[] getVelocityY() {
		return v;
	}
	
	/**
	 * Returns the y velocity at position x/y
	 * @param x
	 * @param y
	 * @return
	 */
	public float getVelocityY(int x, int y) {
		return v[coordToIndex(x, y)];
	}

	/**
	 * Converts coordinates x,y to index position for arrays
	 * @see #getDensity()
	 * @see #getVelocityX()
	 * @see #getVelocityY()
	 * @param x
	 * @param y
	 * @return
	 */
	public int coordToIndex(int x, int y) {
		return x + WIDTH * y;
	}

	private void project(float[] u, float[] v, float[] p, float[] div) {
		int i, j, k;
		float h;
		h = 1.0f / Math.max(X,Y);//(float)Math.sqrt(X*Y);
		int srcIndex = 1+WIDTH;
		for (j = 1; j <= Y; j++) {
			for (i = 1; i <= X; i++) {
				//div[IX(i, j)] = -0.5f
				div[srcIndex] = -0.5f
						* h
						* (
								u[srcIndex+1] - 
								u[srcIndex-1] + 
								v[srcIndex+WIDTH] - 
								v[srcIndex-WIDTH]);
				p[srcIndex] = 0;
				srcIndex++;
			}
			srcIndex+=2;
		}
		
		set_bnd(0, div);
		set_bnd(0, p);
		
		for (k = 0; k < 20; k++) {
			srcIndex = 1+WIDTH;
			for (j = 1; j <= Y; j++) {
				for (i = 1; i <= X; i++) {
					p[srcIndex] = (div[srcIndex] + p[srcIndex-1]
							+ p[srcIndex+1] + p[srcIndex-WIDTH] + p[srcIndex+WIDTH]) / 4;
					srcIndex++;
				}
				srcIndex+=2;
			}
			set_bnd(0, p);
		}
		srcIndex = 1+WIDTH;
		
		float lstp = 0.5f/h;
		for (j = 1; j <= Y; j++) {
			for (i = 1; i <= X; i++) {
				u[srcIndex] -= lstp * (p[srcIndex+1] - p[srcIndex-1]);
				v[srcIndex] -= lstp * (p[srcIndex+WIDTH] - p[srcIndex-WIDTH]);
				srcIndex++;
			}
			srcIndex+=2;
		}
		set_bnd(1, u);
		set_bnd(2, v);
	}

	private void set_bnd(int b, float[] x) {
		int i;
		int srcIndex0 = WIDTH;
		int srcIndex1 = WIDTH+X+1;
		for (i = 1; i <= Y; i++) {
			//x[IX(0, i)] = b == 1 ? -x[IX(1, i)] : x[IX(1, i)];
			//x[IX(X + 1, i)] = b == 1 ? -x[IX(X, i)] : x[IX(X, i)];
			x[srcIndex0] = b == 1 ? -x[srcIndex0+1] : x[srcIndex0+1];
			x[srcIndex1] = b == 1 ? -x[srcIndex1-1] : x[srcIndex1-1];
			srcIndex0 += WIDTH;
			srcIndex1 += WIDTH;
		}
		srcIndex0 = 1;
		srcIndex1 = (Y+1)*WIDTH + 1;
		for (i = 1; i <= X; i++) {	
			//x[IX(i, 0)] = b == 2 ? -x[IX(i, 1)] : x[IX(i, 1)];
			//x[IX(i, Y + 1)] = b == 2 ? -x[IX(i, Y)] : x[IX(i, Y)];
			x[srcIndex0] = b == 2 ? -x[srcIndex0+WIDTH] : x[srcIndex0+WIDTH];
			x[srcIndex1] = b == 2 ? -x[srcIndex1-WIDTH] : x[srcIndex1-WIDTH];
			srcIndex0++;
			srcIndex1++;
		}
		x[coordToIndex(0, 0)] = 0.5f * (x[coordToIndex(1, 0)] + x[coordToIndex(0, 1)]);
		x[coordToIndex(0, Y + 1)] = 0.5f * (x[coordToIndex(1, Y + 1)] + x[coordToIndex(0, Y)]);
		x[coordToIndex(X + 1, 0)] = 0.5f * (x[coordToIndex(X, 0)] + x[coordToIndex(X + 1, 1)]);
		x[coordToIndex(X + 1, Y + 1)] = 0.5f * (x[coordToIndex(X, Y + 1)] + x[coordToIndex(X + 1, Y)]);
	}

	/**
	 * Main step routine
	 * @param inputDensities incoming density array
	 * @param inputVelocitiesX incoming x vector array
	 * @param inputVelocitiesY incoming y vector array
	 * @param diff diffusion
	 * @param visc viscosity
	 * @param dt time step
	 */
	public void step(
			float[] inputDensities, float[] inputVelocitiesX, float[] inputVelocitiesY,
			float diff, float visc, float dt) {
		/*
		 * Diffuse stepper
		 */
		dens_prev = inputDensities;
		u_prev = inputVelocitiesX;
		v_prev = inputVelocitiesY;
		vel_step(u, v, u_prev, v_prev, visc, dt);
		dens_step(dens, dens_prev, u, v, diff, dt);
	}
	
	/**
	 * Main step routine
	 * @param inputDensities incoming density array
	 * @param inputVelocitiesX incoming x vector array
	 * @param inputVelocitiesY incoming y vector array
	 * @param diff diffusion
	 * @param visc viscosity
	 * @param dt time step
	 * @param topClip top value (null: no clipping)
	 */
	public void step(
			float[] inputDensities, float[] inputVelocitiesX, float[] inputVelocitiesY,
			float diff, float visc, float dt, Float topClip) {
		/*
		 * Diffuse stepper
		 */
		dens_prev = inputDensities;
		u_prev = inputVelocitiesX;
		v_prev = inputVelocitiesY;
		vel_step(u, v, u_prev, v_prev, visc, dt);
		dens_step(dens, dens_prev, u, v, diff, dt);
		if (topClip!=null) {
			float val = topClip;
			for (int i=0;i<dens.length;i++) {
				dens[i] = (dens[i]>val)?val:dens[i];
			}
		}
	}

	private void vel_step(
			float[] u, float[] v, 
			float[] u0, float[] v0, 
			float visc, float dt) {
		add_source(u, u0, dt);
		add_source(v, v0, dt);

		float[] tmp = u0;
		u0 = u;
		u = tmp; // SWAP ( u0, u );
		
		diffuse(1, u, u0, visc, dt);
		
		
		tmp = v0;
		v0 = v;
		v = tmp; // SWAP ( v0, v );
		
		diffuse(2, v, v0, visc, dt);
		
		
		project(u, v, u0, v0);
		
		
		tmp = u0;
		u0 = u;
		u = tmp; // SWAP ( u0, u );
		
		tmp = v0;
		v0 = v;
		v = tmp; // SWAP ( v0, v );
		
		advect(1, u, u0, u0, v0, dt);
		advect(2, v, v0, u0, v0, dt);
		
		
		project(u, v, u0, v0);
		
	}

}
