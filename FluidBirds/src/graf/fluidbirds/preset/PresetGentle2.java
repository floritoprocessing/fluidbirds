/**
 * 
 */
package graf.fluidbirds.preset;

/**
 * @author Marcus
 *
 */
public class PresetGentle2 {
	
	public float loResFlowVelocityFac = -255;
	
	public float velocityToDensityFac = 10;
	
	public boolean extraThick = false;
	
	public boolean extraVelocity = true;
	public float extraVelocityX = 0;
	public float extraVelocityY = 0;//-0.001f;
	
	public float[] extraVelocitiesX = null;
	public float[] extraVelocitiesY = null;
	
	public float dt = 1.5f;
	public float diffusion = 0.00001f;   // 0.00001 // lower values -> more small turbulences
	public float viscosity = 0.00004f; // 0.00001 // lower values -> more fluid 'thickness'
	public float evaporation = 0.8f;
	
}
