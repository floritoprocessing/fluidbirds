/**
 * 
 */
package graf.fluidbirds.preset;

import graf.fluidbirds.Fluidbirds;

/**
 * @author Marcus
 *
 */
public class PresetTunnel {
	
	public float loResFlowVelocityFac = -255;
	
	public float velocityToDensityFac = 10;
	
	public boolean extraThick = true;
	
	public boolean extraVelocity = true;
	public float extraVelocityX = 0;
	public float extraVelocityY = 0;
	
	public float[] extraVelocitiesX = null;
	public float[] extraVelocitiesY = null;
	
	public float dt = 3f;
	public float diffusion = 0.0000001f;
	public float viscosity = 0.00001f;
	public float evaporation = 2f;
	
	public PresetTunnel() {
		int w = Fluidbirds.FLUID_WIDTH;
		int h = Fluidbirds.FLUID_HEIGHT;
		
		float cenX = w/2.0f - 0.5f;
		float cenY = h/2.0f - 0.5f;
		
		extraVelocitiesX = new float[w*h];
		extraVelocitiesY = new float[w*h];
		
		int i=0, x, y;
		float dx, dy, fac;
		for (y=0;y<h;y++) {
			for (x=0;x<w;x++) {
				dx = cenX-x;
				dy = cenY-y;
				fac = -(float)(0.0005f/Math.sqrt(dx*dx+dy*dy));
				extraVelocitiesX[i] = dx*fac;
				extraVelocitiesY[i] = dy*fac;
				i++;
			}
		}
		
	}
	
}
