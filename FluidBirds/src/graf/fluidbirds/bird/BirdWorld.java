/**
 * 
 */
package graf.fluidbirds.bird;

import java.util.ArrayList;

import graf.gamefluids.FluidRect;
import processing.core.PApplet;
import processing.core.PVector;

/**
 * @author Marcus
 *
 */
public class BirdWorld {
	
	//private final float fluidToBirdInfluence = 500;
	
	private Bird[] bird = new Bird[800];
	
	/**
	 * A fake 2-dimensional array of ArrayLists describing local neighbourhoods of Birds.
	 */
	private ArrayList<Bird>[] loResWorld;
	private final float toLoResX, toLoResY;
	private final int loResWidth, loResHeight;
	/**
	 * A fake 2-dimensional array of PVectors describing all local average headings
	 */
	private PVector averageHeading[];
	
	private final FluidRect fluid;
	private final float worldToFluidScaleX;
	private final float worldToFluidScaleY;
	
	/*
	 * Temp values
	 */
	private ArrayList<Bird> neighbourhoodForQuad;
	
	public BirdWorld(FluidRect fluid, float maxX, float maxY) {
		this.fluid = fluid;
		worldToFluidScaleX = fluid.WIDTH/maxX;
		worldToFluidScaleY = fluid.HEIGHT/maxY;
		for (int i=0;i<bird.length;i++) {
			bird[i] = new Bird((float)(Math.random()*maxX),(float)(Math.random()*maxY));
		}
		bird[0].debug = true;
		
		
		loResWidth = (int)(maxX/Bird.neighbourhoodRadius);
		loResHeight = (int)(maxY/Bird.neighbourhoodRadius);
		System.out.println("loResWorld: "+loResWidth+"x"+loResHeight);
		toLoResX = loResWidth/maxX;
		toLoResY = loResHeight/maxY;
		loResWorld = new ArrayList[loResWidth*loResHeight];
		averageHeading = new PVector[loResWorld.length];
		for (int i=0;i<loResWorld.length;i++) {
			loResWorld[i] = new ArrayList<Bird>();
			averageHeading[i] = new PVector(0,0);
		}
		
		Bird.maxX = maxX;
		Bird.maxY = maxY;
		
		/*
		 * Init temp values
		 */
		neighbourhoodForQuad = new ArrayList<Bird>(bird.length);
	}

	/**
	 * @param fluid
	 * @param ti
	 */
	public void tick(float ti) {
		
		/*
		 * Create bird neighbourhood:
		 * - Distribute birds in blocks  (loResWorld)
		 */
		
		for (int i=0;i<loResWorld.length;i++) {
			loResWorld[i].clear();
		}
		
		int loResX, loResY, loResIndex;
		for (int i=0;i<bird.length;i++) {
			loResX = Math.max(0, Math.min(loResWidth-1, (int)(bird[i].position.x*toLoResX)));
			loResY = Math.max(0, Math.min(loResHeight-1, (int)(bird[i].position.y*toLoResY)));
			loResIndex = loResX + loResY * loResWidth;
			loResWorld[loResIndex].add(bird[i]);
		}
		
		/*
		 * Create average heading for each quad
		 */
		loResIndex=0;
		for (loResY=0;loResY<loResHeight;loResY++) {
			for (loResX=0;loResX<loResWidth;loResX++) {
				averageHeading[loResIndex].x=0;
				averageHeading[loResIndex].y=0;
				for (Bird b:loResWorld[loResIndex]) {
					averageHeading[loResIndex].x += (b.movement.x/b.currentSpeed);
					averageHeading[loResIndex].y += (b.movement.y/b.currentSpeed);
				}
				averageHeading[loResIndex].normalize();
				loResIndex++;
			}
		}

		/*
		 * Create neighbours for each bird
		 */
		loResIndex=0;
		int xo, yo, nx, ny, indexOffset;
		for (loResY=0;loResY<loResHeight;loResY++) {
			for (loResX=0;loResX<loResWidth;loResX++) {
				
				//ArrayList<Bird> neighbourhoodForQuad = new ArrayList<Bird>();
				neighbourhoodForQuad.clear();
				PVector neighbourhoodHeading = new PVector(0,0);
				if (loResWorld[loResIndex].size()>0) {
					
					/*
					 * create local neighbourhood for this quad (loResWorld[loResIndex]):
					 * create local average heading for this quad
					 */
					for (yo=-1;yo<=1;yo++) {
						indexOffset = yo*loResWidth-1;
						ny = loResY+yo;
						for (xo=-1;xo<=1;xo++) {
							nx = loResX+xo;
							if (nx>=0&&nx<loResWidth&&ny>=0&&ny<loResHeight) { 
								neighbourhoodForQuad.addAll(loResWorld[loResIndex+indexOffset]);
								neighbourhoodHeading.add(averageHeading[loResIndex+indexOffset]);
							}
							indexOffset++;
						}
					}
					neighbourhoodHeading.normalize();
					
					/*
					 * Create local neighbourhoods for each bird
					 * Set neighbourhoodHeading for each bird
					 */
					for (Bird b:loResWorld[loResIndex]) {
						b.neighbourhood.clear();
						b.neighbourhood.addAll(neighbourhoodForQuad);
						b.neighbourhood.remove(b);
						b.neighbourhoodHeading.set(neighbourhoodHeading);
						if (b.debug) {
							for (Bird t:b.neighbourhood) {
								t.tempDebug = true;
							}
						}
					}
					
				}
				
				loResIndex++;
			}
		}
		
		
		
		float birdSpeed;
		
		float rd;
		PVector random = new PVector(0,0);
		
		int fluidX, fluidY, fluidIndex;
		PVector fluidMovement = new PVector(0,0);
		
		for (int i=0;i<bird.length;i++) {
			
			birdSpeed = bird[i].movement.mag();
			
			/*
			 * Random motion
			 */
			rd = (float)(Math.random()*Math.PI*2);
			random.x = (float)(ti * Bird.normSpeed * Bird.randomMovement * Math.cos(rd));
			random.y = (float)(ti * Bird.normSpeed * Bird.randomMovement * Math.sin(rd));
			
			bird[i].movement.add(random);
			//newSpeed = bird[i].movement.mag();
			//bird[i].movement.mult(birdSpeed/newSpeed);
			
			
			/*
			 * Influence bird from fluid
			 */
			
			fluidX = (int)(bird[i].position.x*worldToFluidScaleX);
			fluidY = (int)(bird[i].position.y*worldToFluidScaleY);
			fluidIndex = fluidX + fluidY*fluid.WIDTH;
			fluidMovement.x = ti * Bird.fluidInfluence * fluid.getVelocityX()[fluidIndex];
			fluidMovement.y = ti * Bird.fluidInfluence * fluid.getVelocityY()[fluidIndex];
			bird[i].movement.add(fluidMovement);
			//bird[i].movement.limit(Bird.maxSpeed);	
			
			/*
			 * Limit bird speed and set Bird currentSpeed
			 */
			birdSpeed = bird[i].movement.mag();
			if (birdSpeed<Bird.normSpeed) {
				bird[i].movement.mult(Bird.acceleration);
				birdSpeed *= Bird.acceleration;
			} 
			if (birdSpeed>Bird.maxSpeed) {
				bird[i].movement.mult(Bird.maxSpeed/birdSpeed);
				birdSpeed = Bird.maxSpeed;
			}
			bird[i].currentSpeed = birdSpeed;
			
			
			/*
			 * Influence from local heading
			 */
			PVector birdHeading = new PVector(bird[i].movement.x, bird[i].movement.y);
			birdHeading.div(birdSpeed); // normalize
			birdHeading.mult(1-Bird.alignment);
			PVector neighbourHeading = new PVector(bird[i].neighbourhoodHeading.x, bird[i].neighbourhoodHeading.y);
			neighbourHeading.mult(Bird.alignment);
			PVector newHeading = PVector.add(birdHeading, neighbourHeading);
			newHeading.mult(birdSpeed);
			bird[i].movement = newHeading;
		}
		
		
		/*
		 * Move birds
		 */
		for (int i=0;i<bird.length;i++) {
			
			bird[i].move(ti);
		}
	}

	/**
	 * @param fluidbirds
	 */
	public void draw(PApplet pa) {
		for (int i=0;i<bird.length;i++) {
			bird[i].draw(pa);
		}
	}
}
