/**
 * 
 */
package graf.fluidbirds.bird;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;

/**
 * @author Marcus
 *
 */
public class Bird {
	
	/**
	 * Horizontal max value
	 */
	static float maxX;
	
	/**
	 * Vertical max value
	 */
	static float maxY;
	
	
	
	/**
	 * Normal speed (target) of Bird
	 */
	static float normSpeed = 5;
	
	/**
	 * Maximum speed of Bird
	 */
	static float maxSpeed = 15;
	
	/**
	 * Acceleration of bird to catch up to normSpeed
	 */
	static float acceleration = 1.5f;
	
	/**
	 * Random movement of bird (typically between 0 and 1)
	 */
	static float randomMovement = 0.2f;
	
	/**
	 * Influence of the fluid vector on the bird movement
	 */
	static float fluidInfluence = 200;
	
	
	/**
	 * Influence radius of bird ("neighbourhood")
	 */
	static float neighbourhoodRadius = 50;
	
	/**
	 * Steer to avoid crowding local flockmates
	 */
	static float separation;
	
	/**
	 * Steer toward the average heading (0..1)
	 */
	static float alignment = 0.2f;
	
	/**
	 * Steer to move toward the average position of local flockmates
	 */
	static float cohesion;

	
	
	boolean debug = false;
	boolean tempDebug = false;
	
	PVector position;
	PVector movement;
	float currentSpeed;
		
	ArrayList<Bird> neighbourhood;
	/**
	 * Normalized vector of neighbourhood heading
	 */
	PVector neighbourhoodHeading = new PVector(0,0);
	
	/**
	 * Creates a new Bird at x/y with a random direction
	 * @param x
	 * @param y
	 */
	public Bird(float x, float y) {
		position = new PVector(x,y);
		float rd = (float)(Math.random()*Math.PI*2);
		float xm = (float)(normSpeed*Math.cos(rd));
		float ym = (float)(normSpeed*Math.sin(rd));
		currentSpeed = normSpeed;
		movement = new PVector(xm,ym);
		neighbourhood = new ArrayList<Bird>();
	}

	/**
	 * @param ti
	 */
	public void move(float ti) {
		position.add(PVector.mult(movement, ti));
		if (position.x<0) position.x+=maxX;
		else if (position.x>=maxX) position.x-=maxX;
		if (position.y<0) position.y+=maxY;
		else if (position.y>=maxY) position.y-=maxY;
	}

	/**
	 * @param pa
	 */
	public void draw(PApplet pa) {
		pa.stroke(0);
		if (!debug) {
			if (!tempDebug) {
				pa.fill(255);
			} else {
				pa.fill(0,255,0);
			}
		} else {
			pa.fill(255,0,0);
		}
		pa.ellipseMode(PApplet.RADIUS);
		pa.ellipse(position.x,position.y,5,5);
		pa.line(position.x, position.y, position.x+movement.x, position.y+movement.y);
		tempDebug = false;
	}
}
